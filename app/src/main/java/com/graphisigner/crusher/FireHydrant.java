package com.graphisigner.crusher;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class FireHydrant {


    int xPositionFireHydrant;
    int yPositionFireHydrant;

    Bitmap fireHydrant;

    private Rect hitBox;

    public FireHydrant(Context context, int x, int y) {
        this.fireHydrant = BitmapFactory.decodeResource(context.getResources(), R.drawable.watchtower);

        this.xPositionFireHydrant = x;
        this.yPositionFireHydrant = y;

        this.hitBox = new Rect(this.xPositionFireHydrant, this.yPositionFireHydrant, this.xPositionFireHydrant + this.fireHydrant.getWidth(), this.yPositionFireHydrant + this.fireHydrant.getHeight());

    }

    public void updatePosition() {

        this.xPositionFireHydrant = this.xPositionFireHydrant - GameEngine.bgSpeed;
        // update the position of the hitbox of enemy 2
        this.hitBox.left = this.xPositionFireHydrant;
        this.hitBox.right = this.xPositionFireHydrant + this.fireHydrant.getWidth();

        this.updateHitbox();
    }

    public void updateHitbox() {
        // update the position of the hitbox of enemy 2
        this.hitBox.top = this.yPositionFireHydrant;
        this.hitBox.left = this.xPositionFireHydrant;
        this.hitBox.right = this.xPositionFireHydrant + this.fireHydrant.getWidth();
        this.hitBox.bottom = this.yPositionFireHydrant + this.fireHydrant.getHeight();
    }

    public Rect getHitbox() {
        return this.hitBox;
    }

    public void setxPositionFH(int x) {
        this.xPositionFireHydrant = x;

        this.updateHitbox();
    }
    public void setyPositionFH(int y) {
        this.yPositionFireHydrant = y;

        this.updateHitbox();
    }
    public int getxPositionFireHydrant()
    {
        return this.xPositionFireHydrant;

    }
    public int getyPositionFH() {
        return this.yPositionFireHydrant;
    }

    public Bitmap getBitmapFH()
    {
        return  this.fireHydrant;
    }
    public int getHeightFH() {
        return this.fireHydrant.getHeight();
    }
    public int getWidthFH(){
        return this.fireHydrant.getWidth();
    }



}
