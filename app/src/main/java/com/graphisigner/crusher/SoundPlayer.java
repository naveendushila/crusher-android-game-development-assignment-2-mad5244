package com.graphisigner.crusher;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public class SoundPlayer {

    private SoundPool soundPool;
    private int tankSound;
    private int tankShot;
    private int tankDestroy;

    public SoundPlayer(Context context)
    {
        //soundPool = new SoundPool(5, AudioManager.STREAM_MUSIC,0);
        soundPool = new SoundPool(6, AudioManager.STREAM_MUSIC,0);
        tankSound = soundPool.load(context,R.raw.tank1,2);
        tankShot = soundPool.load(context,R.raw.tankshot,0);
        tankDestroy = soundPool.load(context, R.raw.explode, 1);
    }

    public void playTankSound()
    {

        soundPool.play(tankSound,1.0f,1.0f,1,-1,1.0f);
    }


    public void tankShot()
    {
        soundPool.play(tankShot,1.0f,1.0f,0,0,1.0f);
    }
    public void blastSound()
    {
        soundPool.play(tankDestroy,1.0f,1.0f,0,0,1.0f);
    }

    public void stopSound()
    {
        soundPool.stop(1);

    }
}
