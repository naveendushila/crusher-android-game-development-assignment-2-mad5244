package com.graphisigner.crusher;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class Rocket {


    int xPositionRocket;
    int yPositionRocket;

    Bitmap rocket;

    private Rect hitBox;

    public Rocket(Context context, int x, int y) {
        this.rocket = BitmapFactory.decodeResource(context.getResources(), R.drawable.flymissile);

        this.xPositionRocket = x;
        this.yPositionRocket = y;

        this.hitBox = new Rect(this.xPositionRocket, this.yPositionRocket, this.xPositionRocket + this.rocket.getWidth(), this.yPositionRocket + this.rocket.getHeight());

    }

    public void updatePosition() {

        this.xPositionRocket = this.xPositionRocket - GameEngine.bgSpeed * 4;
        // update the position of the hitbox of enemy 2
        this.hitBox.left = this.xPositionRocket;
        this.hitBox.right = this.xPositionRocket + this.rocket.getWidth();

        this.updateHitbox();
    }

    public void updateHitbox() {
        // update the position of the hitbox of enemy 2
        this.hitBox.top = this.yPositionRocket;
        this.hitBox.left = this.xPositionRocket;
        this.hitBox.right = this.xPositionRocket + this.rocket.getWidth();
        this.hitBox.bottom = this.yPositionRocket + this.rocket.getHeight();
    }

    public Rect getHitbox() {
        return this.hitBox;
    }

    public void setxPositionRocket(int x) {
        this.xPositionRocket = x;

        this.updateHitbox();
    }
    public void setyPositionRocket(int y) {
        this.yPositionRocket= y;

        this.updateHitbox();
    }
    public int getxPositionRocket()
    {
        return this.xPositionRocket;

    }
    public int getyPositionRocket() {
        return this.yPositionRocket;
    }

    public Bitmap getBitmapRocket()
    {
        return  this.rocket;
    }
    public int getHeightRocket() {
        return this.rocket.getHeight();
    }
    public int getWidthRocket(){
        return this.rocket.getWidth();
    }
    public void setBitmap(Context context, int imageCode) {
        if (imageCode == 1) {
            this.rocket = BitmapFactory.decodeResource(context.getResources(), R.drawable.flymissile);
        }
        else {
            this.rocket = BitmapFactory.decodeResource(context.getResources(), R.drawable.flymissile1);
        }
    }
}
