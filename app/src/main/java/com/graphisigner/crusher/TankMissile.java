package com.graphisigner.crusher;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.util.Log;

public class TankMissile {


    int xPosition;
    int yPosition;

    int direction;
    Bitmap missile;

    int height;
    int width;


    private Rect hitBox;

    public TankMissile(Context context, int x, int y) {
        this.missile = BitmapFactory.decodeResource(context.getResources(), R.drawable.bomb);
        this.xPosition = x;
        this.yPosition = y;
        this.hitBox = new Rect(this.xPosition, this.yPosition, this.xPosition + this.missile.getWidth(), this.yPosition + this.missile.getHeight());
    }

    public void fire() {
        this.xPosition = this.xPosition + 175;
        this.updateHitbox();
        Log.d("Fire", "Fire X Position" + this.xPosition);
    }

    public void updateHitbox() {
        // update the position of the hitbox of enemy 1
        this.hitBox.top = this.yPosition;
        this.hitBox.left = this.xPosition;
        this.hitBox.right = this.xPosition + this.missile.getWidth();
        this.hitBox.bottom = this.yPosition + this.missile.getHeight();
    }

    public Rect getHitbox() {
        return this.hitBox;
    }


    public void setxPosition(int x) {
        this.xPosition = x;

        this.updateHitbox();
    }
    public void setyPosition(int y) {
        this.yPosition = y;

        this.updateHitbox();
    }

    public int getxPosition()
    {
        return this.xPosition;

    }
    public int getyPosition() {
        return this.yPosition;
    }

    public Bitmap getBitmap() {
        return this.missile;
    }

    public int getHeight() {
        return this.missile.getHeight();
    }
    public int getWidth(){
        return this.missile.getWidth();
    }


}
