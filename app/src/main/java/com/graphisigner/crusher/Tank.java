package com.graphisigner.crusher;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.Log;

public class Tank {

    int xPosition;
    int yPosition;
    int direction = -1;              // -1 = not moving, 0 = down, 1 = up
    Bitmap playerImage;
    int height;
    int width;

    int z;
    double counter = 4;

    float velocityX;
    float velocityY ;
    //= -12.0f;
    float gravity = 0.5f;
    int time = 15;

    Thread jumpThread;

    final int SQUARESPEED = 30;

    private Rect hitBox;
    public Tank(Context context, int x, int y ) {

        this.playerImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.tank1);

        this.xPosition = x;
        this.yPosition = y;


        height = this.playerImage.getHeight();
        width = this.playerImage.getWidth();

        this.hitBox = new Rect(this.xPosition, this.yPosition, this.xPosition + this.playerImage.getWidth(), this.yPosition + this.playerImage.getHeight());

    }

    public void gameEndPlayerMove() {

            // move up
            this.xPosition = this.xPosition + 50;

        // update the position of the hitbox
        this.updateHitbox();
    }

    public void jump() {
        if (this.direction == 1)
        {
            // move down
            this.yPosition = this.yPosition - 125;
        }
        else if (this.direction == 0)
        {
            // move up
            this.yPosition = this.yPosition + 125;
        }

        // update the position of the hitbox
        this.updateHitbox();
    }

    public void updateHitbox()
    {
        // update the position of the hitbox
        this.hitBox.top = this.yPosition;
        this.hitBox.left = this.xPosition;
        this.hitBox.right = this.xPosition + this.playerImage.getWidth();
        this.hitBox.bottom = this.yPosition + this.playerImage.getHeight();
    }

    public Rect getHitbox() {
        return this.hitBox;
    }

    public void setXPosition(int x) {
        this.xPosition = x;
        this.updateHitbox();
    }
    public void setYPosition(int y) {
        this.yPosition = y;
        this.updateHitbox();
    }
    public int getXPosition() {
        return this.xPosition;
    }
    public int getYPosition() {
        return this.yPosition;
    }
    public int getHeight() {
        return this.height;
    }
    public int getWidth(){
        return this.width;
    }

    public void setDirection(int i) {
        this.direction = i;
    }
    public Bitmap getBitmap() {
        return this.playerImage;
    }
    public void setBitmap(Context context, int imageCode) {
        if (imageCode == 1) {
            this.playerImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.tank1);
        }
        else {
            this.playerImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.tank2);
        }
    }

}
