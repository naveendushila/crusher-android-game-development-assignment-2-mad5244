package com.graphisigner.crusher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // threading
    Thread gameThread;

    // ----------------------------
    // ## GAME GRID (coordinate system)
    // ----------------------------
    static int blockSize;
    private final int NUM_BLOCKS_WIDE = 20;
    private int numBlocksHigh;
    static int maxX = 0;
    static int maxY = 0;
    static int minX = 0;
    static int minY = 0;

    // screen size
    int screenHeight;
    int screenWidth;

    // Android debug variables
    final static String TAG = "CRUSHER";

    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;

    //Drawing Variables
    boolean drawSpriteHitboxes = false;
    boolean drawGridLines = false;


    //Background
    Bitmap background;
    Bitmap backgroundClouds;

    int bgX_Trees = 0;
    int bgRight_Trees;

    int bgX_Clouds = 0;
    int bgRight_clouds;

    // game state
    boolean gameIsRunning;
    boolean tankJumping = false;
    int tankDefault = 0;
    boolean gameEndCheck = false;

    int timeCounter = 0;
    static int numberDestroy = 0;
    static int maxDestroy = GameStart.Score;
    boolean playingFirst = true;
    boolean playerDead = false;

    int tankImgCode = 1;
    boolean fireMissile = false;
    boolean fireIsRunning = false;

    static int bgSpeed = 25;
    static int bgCloudSpeed = (int) (bgSpeed / 4);


    // ----------------------------
    // ## SPRITES
    // ----------------------------
    Tank tank;
    Enemy enemy;
    FireHydrant watchTower;
    Rocket rocket;
    TankMissile tankMissile;



    // variable for sounds
    // ------------------

    public SoundPlayer sound = new SoundPlayer(getContext());

    public GameEngine(Context context, int w, int h) {

        super(context);
        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;


        //@TODO: Divide the screen into blocks so that the UI looks same on all devices
        blockSize = this.screenWidth / this.NUM_BLOCKS_WIDE;
        this.numBlocksHigh = this.screenHeight / blockSize;

        this.printScreenInfo();

        // SET PLAYFIELD BOUNDARIES
        minX = 1;
        minY = 1;
        maxX = NUM_BLOCKS_WIDE;
        maxY = numBlocksHigh;

        // @TODO: Add sprites (More enemies into the level of player)
        this.spawnPlayer();
        this.spawnEnemyShips();


        // @TODO: Any other game setup

        background = BitmapFactory.decodeResource(context.getResources(), R.drawable.trees_bg);
        background = Bitmap.createScaledBitmap(background, this.screenWidth, this.screenHeight, false);
        bgRight_Trees = background.getWidth();

        backgroundClouds = BitmapFactory.decodeResource(context.getResources(), R.drawable.clouds);
        backgroundClouds = Bitmap.createScaledBitmap(backgroundClouds, this.screenWidth, this.screenHeight, false);
        bgRight_clouds = backgroundClouds.getWidth();

    }

    private void printScreenInfo() {
        //@TODO: Save the number of Lives and  Scores
        Log.d(TAG, "Block Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
        Log.d(TAG, "Block Size: " + blockSize + "px");
        Log.d(TAG, "Blocks High: " + this.numBlocksHigh);
        Log.d(TAG, "Blocks Wide: " + this.NUM_BLOCKS_WIDE);
        Log.d(TAG, "Screen h x w = " + this.screenHeight + "," + this.screenWidth);
    }


    private void spawnPlayer() {

        //@TODO: Spawn player at the level
        tank = new Tank(this.getContext(), ((minX + 1) * blockSize), ((maxY - 3) * blockSize));
        tankDefault = ((maxY - 3) * blockSize);
        tankMissile = new TankMissile(this.getContext(), ((minX - 2) * blockSize), minY);
    }


    private void spawnEnemyShips() {
        //Spawning Moving object
        enemy = new Enemy(this.getContext(), ((maxX + 1) * blockSize), ((maxY - 3) * blockSize));

        //spawing stationary obstacle
        watchTower = new FireHydrant(this.getContext(), this.screenWidth * 2, ((maxY - 4) * blockSize));

        //spawning Random Obstacle
        rocket = new Rocket(this.getContext(), this.screenWidth * 3, ((minY + 2) * blockSize));
    }


    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        bgSpeed = 25;
        bgCloudSpeed = (int) (bgSpeed / 4);
        while (gameIsRunning == true) {
            timeCounter++;
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }

    public void pauseGame() {
        gameIsRunning = false;
        sound.stopSound();
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

     public void restartGame() {
        sound.playTankSound();
        playerDead = false;
        gameIsRunning = true;
        playingFirst = false;
        try {
            gameThread.start();
        }
        catch (Exception e) {
            //Error
        }

        gameThread = new Thread(this);
        gameThread.start();

    }

    public void startGame() {
        sound.playTankSound();
        playerDead = false;
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();

    }

    public void gameEnd() {
        gameIsRunning = false;
        timeCounter = 0;
        sound.stopSound();
        GameStart.Score = numberDestroy;
        numberDestroy = 0;

        Intent saveHighScore = new Intent().setClass(this.getContext(), SaveHighScore.class);
        ((Activity) getContext()).startActivity(saveHighScore);
    }

    /*
        Function to run once player hit with any collision
    */

    public void playerHitEnemy() {

        gameIsRunning = false;
        sound.stopSound();
        Log.d("Scores", "numberDestroy" + numberDestroy);
        Log.d("Scores", "Game Start Score" + GameStart.Score);

        GameStart.currentPlayerScore = numberDestroy;

        if (numberDestroy <= GameStart.Score)
        {
            Intent restartGame = new Intent().setClass(this.getContext(), restartGame.class);
            ((Activity) getContext()).startActivity(restartGame);

        }
        else {
            GameStart.Score = numberDestroy;
            Intent saveHighScore = new Intent().setClass(this.getContext(), SaveHighScore.class);
            ((Activity) getContext()).startActivity(saveHighScore);
        }
        playerDead = true;
        timeCounter = 0;
        numberDestroy = 0;
    }



    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    public void updatePositions() {


        /*
            -Making game harder and increase the speed of the game
         */

        if (timeCounter > 250 && timeCounter < 400) {
            bgSpeed = 30;
            bgCloudSpeed = (int) (bgSpeed / 4);
        }
        else if (timeCounter > 400 && timeCounter < 550) {
            bgSpeed = 35;
            bgCloudSpeed = (int) (bgSpeed / 4);
        }
        else if (timeCounter > 550 && timeCounter <650) {
            bgSpeed = 40;
            bgCloudSpeed = (int) (bgSpeed / 4);
        }
        else if(timeCounter > 650 && timeCounter < 750) {
            bgSpeed = 45;
            bgCloudSpeed = (int) (bgSpeed / 4);
        }
        else if (timeCounter > 750 && timeCounter < 800) {
            bgSpeed = 50;
            bgCloudSpeed = (int) (bgSpeed / 4);
        }
        else if (timeCounter >= 1000) {
            gameEndCheck = true;
            this.tankMissile.setxPosition(screenWidth + 250);
            this.enemy.setxPositionTank(screenWidth + 250);
            this.watchTower.setxPositionFH(screenWidth + 250);
            this.rocket.setxPositionRocket(screenWidth + 250);
        }

        if(gameEndCheck == false) {

        /*
        Making tank jump
         */
            this.tank.jump();

            if (tankJumping == true) {

                if (tank.yPosition <= minY + 50) {
                    this.tank.direction = 0;
                } else if (tank.yPosition >= tankDefault) {
                    this.tank.direction = -1;
                    this.tank.yPosition = tankDefault;
                    tankJumping = false;
                }

            }

        /*
        Making player tank move
        */

            if (tankImgCode == 1) {
                tank.setBitmap(this.getContext(), tankImgCode);
                tankImgCode = 2;
            } else {
                tank.setBitmap(this.getContext(), tankImgCode);
                tankImgCode = 1;
            }


        /*
        parallax backround
         */
            bgX_Trees = bgX_Trees - bgSpeed;

            bgRight_Trees = bgRight_Trees - bgSpeed;

            bgX_Clouds = bgX_Clouds - bgCloudSpeed;
            bgRight_clouds = bgRight_clouds - bgCloudSpeed;

            if (bgRight_Trees <= 0) {
                bgX_Trees = 0;
                bgRight_Trees = background.getWidth();
            }

            if (bgRight_clouds <= 0) {
                bgX_Clouds = 0;
                bgRight_clouds = backgroundClouds.getWidth();

            }


        /*
        Eneny postion uodation
         */
            enemy.updateEnemyPositionTank();
            watchTower.updatePosition();


        /*
        Collision Detection and Update positions
         */

            // @TODO: Collision detection between enemy and wall
            if (enemy.getxPositionTank() + enemy.getHeightTank() <= 0) {
                enemy.setxPositionTank(((maxX + 1) * blockSize));

            }

            // @TODO: Collision detection between tank and enemy
            if (tank.getHitbox().intersect(enemy.getHitbox())) {
                Log.d("Crusher", "collision between tank and enemy tank");
                playerHitEnemy();
                tank.updateHitbox();
            }


            if (watchTower.getxPositionFireHydrant() + watchTower.getWidthFH() <= 0) {
                //reset the enemy's starting position to right side of screen
                // you may need to adjust this number according to your device/emulator
                watchTower.setxPositionFH(this.screenWidth * 2);

            }

            // @TODO: Collision detection between tank and enemy watchTower Hydrant
            if (tank.getHitbox().intersect(watchTower.getHitbox())) {
                Log.d("Crusher", "collision between tank and watchTower hydrant");
                playerHitEnemy();
                tank.updateHitbox();

            }

            rocket.updatePosition();

            if (rocket.getxPositionRocket() + rocket.getWidthRocket() <= 0) {

                getRocketRandomly();

            }


            // @TODO: Collision detection between tank and enemy watchTower Hydrant
            if (tank.getHitbox().intersect(rocket.getHitbox())) {
                Log.d("Crusher", "collision between tank and Rocket");
                playerHitEnemy();
                tank.updateHitbox();

            }



        /*
        Firing missile - Enemy Destruction

         */
            //@TODO: Check the watchTower missile

            if (fireMissile == true) {

                if (fireIsRunning == false) {
                    tankMissile.setxPosition(GameEngine.minX + 3 * GameEngine.blockSize);
                    tankMissile.setyPosition(this.tank.getYPosition() + 10);
                    fireIsRunning = true;
                } else {
                    if (tankMissile.xPosition + tankMissile.getWidth() <= screenWidth) {
                        tankMissile.fire();
                    } else {
                        resetMissile();

                    }
                }

            }


        /*
        Collision detection with missile and enemy
         */

            if (tankMissile.getHitbox().intersect(enemy.getHitbox())) {
                enemy.setxPositionTank(this.screenWidth * 2);
                sound.blastSound();
                resetMissile();
                numberDestroy++;
            }

            if (tankMissile.getHitbox().intersect(watchTower.getHitbox())) {
                watchTower.setxPositionFH(this.screenWidth * 2);
                sound.blastSound();
                resetMissile();
                numberDestroy++;
            }

            if (tankMissile.getHitbox().intersect(rocket.getHitbox())) {
                getRocketRandomly();
                sound.blastSound();
                resetMissile();
                numberDestroy++;
            }
        }
        else {
            if (this.tank.getXPosition() + this.tank.getWidth() >= screenWidth) {
                gameEnd();
            }
            else {
                this.tank.xPosition = this.tank.xPosition + 25;
            }
        }
    }

    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {

            this.canvas = this.holder.lockCanvas();

            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255, 255, 255, 255));
            paintbrush.setColor(Color.WHITE);

            canvas.drawBitmap(this.backgroundClouds, bgX_Clouds, 0, paintbrush);
            canvas.drawBitmap(this.backgroundClouds, bgRight_clouds, 0, paintbrush);

            // Draw background
            canvas.drawBitmap(this.background, bgX_Trees, 0, paintbrush);
            canvas.drawBitmap(this.background, bgRight_Trees, 0, paintbrush);

            //@TODO: Draw the tank
            canvas.drawBitmap(this.tank.getBitmap(), this.tank.getXPosition(), this.tank.getYPosition(), paintbrush);

            //@TODD: Draw the missile
            canvas.drawBitmap(this.tankMissile.getBitmap(), this.tankMissile.getxPosition(), this.tankMissile.getyPosition(), paintbrush);

            //@TODO: Draw the enemy tank
            canvas.drawBitmap(this.enemy.getBitmapTank(), this.enemy.getxPositionTank(), this.enemy.getyPositionTank(), paintbrush);

            //@TODO: Draw the Hydrant
            canvas.drawBitmap(this.watchTower.getBitmapFH(), this.watchTower.getxPositionFireHydrant(), this.watchTower.getyPositionFH(), paintbrush);

            //@TODO: Draw the Rocket
            canvas.drawBitmap(this.rocket.getBitmapRocket(), this.rocket.getxPositionRocket(), this.rocket.getyPositionRocket(), paintbrush);

            //@TODO: Draw the missile
            canvas.drawBitmap(this.tankMissile.getBitmap(), this.tankMissile.getxPosition(), this.tankMissile.getyPosition(), paintbrush);


            if (drawSpriteHitboxes == true) {
                // Show the hitboxes on tank and enemy
                paintbrush.setColor(Color.BLUE);
                paintbrush.setStyle(Paint.Style.STROKE);
                paintbrush.setStrokeWidth(5);
                Rect tankHitbox = tank.getHitbox();
                canvas.drawRect(tankHitbox.left, tankHitbox.top, tankHitbox.right, tankHitbox.bottom, paintbrush);

                Rect enemyHitbox = enemy.getHitbox();
                canvas.drawRect(enemyHitbox.left, enemyHitbox.top, enemyHitbox.right, enemyHitbox.bottom, paintbrush);

                Rect fireHydrantHitbox = watchTower.getHitbox();
                canvas.drawRect(fireHydrantHitbox.left, fireHydrantHitbox.top, fireHydrantHitbox.right, fireHydrantHitbox.bottom, paintbrush);

                Rect RocketHitbox = rocket.getHitbox();
                canvas.drawRect(RocketHitbox.left, RocketHitbox.top, RocketHitbox.right, RocketHitbox.bottom, paintbrush);

                Rect tankMissileHitbox = tankMissile.getHitbox();
                canvas.drawRect(tankMissileHitbox.left, tankMissileHitbox.top, tankMissileHitbox.right, tankMissileHitbox.bottom, paintbrush);
            }

            // draw game stats
            paintbrush.setTextSize(45);
            paintbrush.setColor(Color.BLACK);
            canvas.drawText("Timer: " + timeCounter, 100, 80, paintbrush);

            // draw game stats
            canvas.drawText("Max Destroy: " + GameStart.Score, 100, 150, paintbrush);

            canvas.drawText("Enemy Destroy: " + numberDestroy, 1700, 80, paintbrush);


            //-------Grid Style---------
            if (drawGridLines == true) {
                paintbrush.setStyle(Paint.Style.STROKE);
                paintbrush.setStrokeWidth(2);
                paintbrush.setColor(Color.BLACK);
                for (int i = 0; i < this.NUM_BLOCKS_WIDE; i++) {
                    canvas.drawLine(i * blockSize, 0, i * blockSize, numBlocksHigh * blockSize, paintbrush);
                }

                for (int i = 0; i < this.numBlocksHigh; i++) {
                    canvas.drawLine(0, i * blockSize, NUM_BLOCKS_WIDE * blockSize, i * blockSize, paintbrush);
                }

            }

            this.holder.unlockCanvasAndPost(canvas);

        }
    }


    public void setFPS() {
        try {
            gameThread.sleep(35);
        } catch (Exception e) {

        }
    }


    //---------------Random posiions for rocket

    public void getRocketRandomly() {
        //reset the enemy's starting position to right side of screen
        // you may need to adjust this number according to your device/emulator
        Random r = new Random();
        double rand = (Math.random() * ((this.screenWidth * 4 - this.screenWidth * 2) + 1)) + this.screenWidth;
        int randomYPos = r.nextInt(this.screenHeight - (2 * blockSize));
        int randomXPos = (int) (rand);
        Log.d("Misssile Position", "New Random X:" + randomXPos);

        rocket.setxPositionRocket(randomXPos);
        rocket.setyPositionRocket(randomYPos);
    }

    public void resetMissile() {
        fireIsRunning = false;
        fireMissile = false;
        tankMissile.setxPosition(((minX - 2) * blockSize));
        tankMissile.setyPosition(minY);
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        //@TODO: Handling different user inputs to jump and numberDestroy things


        float locationX = event.getX();
        float locationY = event.getY();
//        Log.d("Tap Location", "Value of X:" + locationX);
//        Log.d("Tap Location", "Value of Y:" + locationY);

        int userAction = event.getActionMasked();

        //if (gameIsRunning == false && playerDead == true) {
        if (gameIsRunning == false) {
            if (userAction == MotionEvent.ACTION_DOWN) {
                timeCounter = 0;
                tank.updateHitbox();
                Log.d("Game Play", "Screen Tapped");
                //this.startGame();
                restartGame();
            }
            return true;
        }
        else {
            if (locationX < screenWidth / 2) {

                if (userAction == MotionEvent.ACTION_DOWN) {
                    Log.d("User Tap", "Left");
                    if (!tankJumping) {
                        tank.direction = 1;
                        tankJumping = true;
                        tank.jump();

                    }
                    return true;
                }

            } else {
                if (userAction == MotionEvent.ACTION_DOWN) {
                    if (fireMissile != true) {
                        Log.d("User Tap", "Right");
                        fireMissile = true;
                        sound.tankShot();
                        return true;
                    }

                }

            }

        }
        return true;
    }
}