package com.graphisigner.crusher;

import android.arch.lifecycle.ViewModelStoreOwner;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class HighScorePlayer extends AppCompatActivity implements View.OnClickListener {

    TextView highScores;
    TextView highScorePlayer;
    Button backBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.highscoreplayer);

        backBtn = (Button) findViewById(R.id.goBack);

        backBtn.setOnClickListener(this);

        highScores = (TextView) findViewById(R.id.HighScoreNumber);
        highScorePlayer = (TextView) findViewById(R.id.HighScorePlayer);


        highScores.setText("Max Score: " + String.valueOf(GameStart.Score));
        highScorePlayer.setText("Player Name: " + GameStart.player);

    }

    @Override
    public void onClick(View view) {

        if(view.getId() == backBtn.getId()) {
            finish();
            Intent startGameIntent = new Intent(this, GameStart.class);
            startActivity(startGameIntent);
        }

    }
}
