package com.graphisigner.crusher;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class Enemy {

    int xPositionTank;
    int yPositionTank;

    int direction;
    Bitmap enemyTank;

    int height;
    int width;


    private Rect hitBox;

    public Enemy(Context context, int x, int y) {
        this.enemyTank = BitmapFactory.decodeResource(context.getResources(), R.drawable.enemy);
        this.xPositionTank = x;
        this.yPositionTank = y;
        this.hitBox = new Rect(this.xPositionTank, this.yPositionTank, this.xPositionTank + this.enemyTank.getWidth(), this.yPositionTank + this.enemyTank.getHeight());
    }


    public void updateEnemyPositionTank() {

        this.xPositionTank = this.xPositionTank - GameEngine.bgSpeed * 3;
        // update the position of the hitbox of enemy 1
        this.hitBox.left = this.xPositionTank;
        this.hitBox.right = this.xPositionTank + this.enemyTank.getWidth();
        this.updateHitbox();
    }

    public void updateHitbox() {
        // update the position of the hitbox of enemy 1
        this.hitBox.top = this.yPositionTank;
        this.hitBox.left = this.xPositionTank;
        this.hitBox.right = this.xPositionTank + this.enemyTank.getWidth();
        this.hitBox.bottom = this.yPositionTank + this.enemyTank.getHeight();
    }

    public Rect getHitbox() {
        return this.hitBox;
    }


    public void setxPositionTank(int x) {
        this.xPositionTank = x;

        this.updateHitbox();
    }
    public void setyPositionTank(int y) {
        this.yPositionTank = y;

        this.updateHitbox();
    }

    public int getxPositionTank()
    {
        return this.xPositionTank;

    }
    public int getyPositionTank() {
        return this.yPositionTank;
    }

    public Bitmap getBitmapTank() {
        return this.enemyTank;
    }

    public int getHeightTank() {
        return this.enemyTank.getHeight();
    }
    public int getWidthTank(){
        return this.enemyTank.getWidth();
    }


}
