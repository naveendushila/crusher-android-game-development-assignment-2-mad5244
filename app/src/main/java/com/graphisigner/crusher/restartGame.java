package com.graphisigner.crusher;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class restartGame extends AppCompatActivity implements View.OnClickListener{

    Button restartGame, exitGame;
    TextView playerScore;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restart);
        playerScore = (TextView) findViewById(R.id.playerScore);
        restartGame = (Button) findViewById(R.id.restartGame);
        exitGame = (Button) findViewById(R.id.endRestartGame);

        playerScore.setText("Your Score:" + String.valueOf(GameStart.currentPlayerScore));

        restartGame.setOnClickListener(this);
        exitGame.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        if (view.getId() == restartGame.getId()) {
            Intent startGameIntent = new Intent(this, MainActivity.class);
            startActivity(startGameIntent);
        }
        else if (view.getId() == exitGame.getId()) {
            finish();
            Intent startGameIntent = new Intent(this, GameStart.class);
            startActivity(startGameIntent);
        }


    }
}
