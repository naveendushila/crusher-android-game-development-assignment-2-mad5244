package com.graphisigner.crusher;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.SoundPool;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    GameEngine tappySpaceship;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get size of the screen
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        // Initialize the GameEngine object
        // Pass it the screen size (height & width)
        tappySpaceship = new GameEngine(this, size.x, size.y);

        // Make GameEngine the view of the Activity
        setContentView(tappySpaceship);
    }

    // Android Lifecycle function
    @Override
    protected void onResume() {
        super.onResume();
        tappySpaceship.startGame();

    }

    // Stop the thread in snakeEngine
    @Override
    protected void onPause() {
        super.onPause();
        tappySpaceship.pauseGame();
    }
}
