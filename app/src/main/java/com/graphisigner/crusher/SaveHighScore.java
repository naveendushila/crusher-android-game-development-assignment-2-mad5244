package com.graphisigner.crusher;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class SaveHighScore extends AppCompatActivity implements View.OnClickListener {

    Button saveHighScore;
    EditText playerName;
    TextView playerScore;
    int Score = 0;

    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.highscore);

        Score = GameStart.Score;

        playerScore = (TextView) findViewById(R.id.playerScore);

        playerName = (EditText) findViewById(R.id.highScorePlayer);

        saveHighScore = (Button) findViewById(R.id.saveHighScore);
        saveHighScore.setOnClickListener(this);

        playerScore.setText("Max Score: " + String.valueOf(Score));

        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPref.edit();

    }

    @Override
    public void onClick(View view) {

        if(view.getId() == saveHighScore.getId()) {
            String player = playerName.getText().toString();

            if (player.equals("")) {
                Toast.makeText(this, "Please enter player name!" , Toast.LENGTH_SHORT).show();
            }
            else {

                //sharedPref = this.getPreferences(Context.MODE_PRIVATE);


                editor.putInt(GameStart.highScorenumber, Score);
                editor.putString(GameStart.highScorePlayer, player);
                editor.commit();

                Toast.makeText(this, "High Score Updated!" , Toast.LENGTH_SHORT).show();

                finish();
                Intent startGameIntent = new Intent(this, GameStart.class);
                startActivity(startGameIntent);

            }
        }

    }

}
