package com.graphisigner.crusher;

import android.app.AppComponentFactory;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

public class launcher extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.laucher);

        final Context context;

        context = this;

        Thread timer = new Thread(){
            public void run(){
                try{
                    sleep( 3000);
                } catch (Exception ex) {
                    Log.e("Launcher", "Thread wait failed");
                }
                finally {
                    finish();
                    Intent loginIntent = new Intent(context, GameStart.class);
                    startActivity(loginIntent);
                }

            }
        };
        timer.start();
    }

}
