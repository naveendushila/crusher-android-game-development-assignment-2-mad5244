package com.graphisigner.crusher;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class GameStart extends AppCompatActivity implements View.OnClickListener {

    Button startGame, highScore, exitGame;

    static String highScorenumber = "HighScore";
    static String highScorePlayer = "HighScorePlayer";

    static int Score = 0;
    static String player = "";

    static int currentPlayerScore = 0;

    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_start);
        startGame = (Button) findViewById(R.id.playGame);
        highScore = (Button) findViewById(R.id.highScore);
        exitGame = (Button) findViewById(R.id.endGame);


        startGame.setOnClickListener(this);
        highScore.setOnClickListener(this);
        exitGame.setOnClickListener(this);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        player = sharedPref.getString(GameStart.highScorePlayer,"No High Score Information Yet!");
        Score = sharedPref.getInt(GameStart.highScorenumber, 00);

    }

    @Override
    public void onClick(View view) {

        if(view.getId() == startGame.getId() ) {

            Intent startGameIntent = new Intent(this, MainActivity.class);
            startActivity(startGameIntent);

        }
        else if (view.getId() == highScore.getId())
        {

            Intent highScoreIntent = new Intent(this, HighScorePlayer.class);
            startActivity(highScoreIntent);
        }
        else if (view.getId() == exitGame.getId()) {
//            Toast.makeText(this, "End Game!" , Toast.LENGTH_SHORT).show();
            finish();
            moveTaskToBack(true);
        }

    }
}
